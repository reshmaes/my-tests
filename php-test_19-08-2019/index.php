<?php
include_once("header.php");
include_once('class/Login.class.php');

$objLogin = new Login();

switch($objLogin->setAction){
    
        case "":{?>
        <h2>Welcome Page</h2><br>
            <a href="home.php" class="btn btn-primary" role="button">View</a>
             
                <a class="btn btn-primary" href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?setAction=register";?>" role="button">Register</a>
                <a class="btn btn-primary" href="<?php echo htmlspecialchars($_SERVER["PHP_SELF"])."?setAction=search";?>"role="button">Search</a>
            </form>
            </div>
            <?php
        }
          break;

        case "register":{
                ?><div class="container">
                    <h1> Register User </h1>
                    
                        <form name="f1" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" onsubmit="javascript: return jsValidation(this);">

                            <div class="form-group">
                                
                                <input type="textarea" name="userName" placeholder="UserName" class="form-control" id="uname">
                            </div>
                            <div class="form-group">
                               
                                <input type="email" name="userEmail" placeholder="Email" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                               
                                <input type="password" name="userPass" placeholder="Password" class="form-control" id="pwd">
                            </div>
                            <div class="form-group">
                                <label for="dob">Date of Birth:</label>
                                <input type="date" name="userDob" class="form-control" id="dob">
                            </div>
                            <div class="form-group">
                          
                                <input type="textarea" name="userPhone" placeholder="Phone Number" class="form-control" id="phone">
                            </div>
                           
                         
                            <input type="hidden" name="setAction" value="registerUser">
                            <button type="submit" class="btn btn-default">Register</button>
                        </form>
                   <?php
                        if(isset($objLogin->message) && !empty($objLogin->message)){
                            ?><div class="alert <?php print($objLogin->class);  ?>"><?php
                                print($objLogin->message);
                            ?></div><?php
                        }
                ?>
                </div>
                <script>
                function jsValidation(objForm) {
                   
                    if (trimfield(objForm.userName.value) == '') {
                        alert("Name Required!");
                        objForm.userName.focus()
                        return false;
                    }
                    if (!validateEmail(trimfield(objForm.userEmail.value))) {
                        alert("Valid Email Required!");
                        objForm.userEmail.focus()
                        return false;
                    }
                    if (trimfield(objForm.userPass.value) == '') {
                        alert("Valid Password Required!");
                        objForm.userPass.focus()
                        return false;
                    }
                    if(isNaN(objForm.userPhone.value)){
                        alert("Numeric values allowed");
                        objForm.userPhone.focus();
                        return false;
                    }
                    $phone = strlen(objForm.userPhone.value);
                     if ($phone < 10 || $phone > 15) {
                        alert("Valid phonenumber Required!");
                        objForm.userPhone.focus()
                        return false;
                    }
                    if (trimfield(objForm.userPhone.value) == '') {
                    alert("Valid phone  number Required!");
                    objForm.userPass.focus()
                    return false;
                } 

                    return true;
                }
                function validateEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(String(email).toLowerCase());
                }
                function trimfield(str) {
                    return str.replace(/^\s+|\s+$/g, '');
                }
                if (userDob.value.trim() == ""){                                  
                    alert("Please enter date"); 
                    userDob.focus(); 
                    return false; 
                }
                </script>
        
                <?php
            } break;

         case "search":?>
             <html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
 </head>
 <body>
  <div class="container">
   <br />
   <h2 align="center">Search User</h2><br />
   <div class="form-group">
    <div class="input-group">
     <span class="input-group-addon">Search</span>
     <input type="text" name="search_text" id="search_text" placeholder="Search by User Name" class="form-control" />
    </div>
   </div>
   <br />
   <div id="result"></div>
  </div>
 </body>
</html>


<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"search.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
<?php
    break;
    default : 
        ?><div class="alert alert-warning" role="alert">   
        <p>Sorry  Page Not Found!</p>
        <hr>
        <p class="mb-0">Please check !</p>
        </div>
        <?php
 }
 ?>
 