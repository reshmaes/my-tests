............ Assessment Test Question ............

 Create a user list with the details
    1. Email:- Should be unique
    2. Password:- Encrypted
    3. DOB:- Should have a date picker 
    4. Name:-  Should only have characters
    5. Phone Number:- Only have numbers
                a) Option to add user
                b) Edit user
                c) List user
                d) Search user by ajax