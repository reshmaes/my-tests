<?php
include_once('DBResult.class.php');
class Login {

    public $setAction = "";
    public $message = "";
    public $class = "";
    public $dbObj = "";
    function __construct(){

        if(isset($_REQUEST['setAction'])){
            $this->setAction = $_REQUEST['setAction'];
        }

       $this->dbObj = new DBResult();
      
        switch($this->setAction){
            
            case "registerUser": {
                if($this->dbObj->checkUserRegister($_POST)){
                   
                    $this->message = " user registered successfully!";
                    $this->class = "alert-success";
                } 
                else {
                    $this->message = "can not register!";
                    $this->class = "alert-danger";
                }
                $this->setAction = "";
                break;
            }
            case "searchUser":{
                if($this->dbObj->searchUser($_POST)){
                   
                    $this->message = " user found !";
                    $this->class = "alert-success";
                } 
                else {
                    $this->message = "can not find!";
                    $this->class = "alert-danger";
                }
                $this->setAction = "";
                break;
            }
        
    }

}

}
?>
