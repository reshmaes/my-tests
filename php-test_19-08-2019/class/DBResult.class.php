<?php
require_once __DIR__ . '/../config/config.php';

class DBResult
{

    private $connection;

    function __construct()
    {

        $this->connection = mysqli_connect(HOST, DB_USERNAME, DB_PASSWORD, DATABASE);
    }

    function __destruct()
    {
        mysqli_close($this->connection);
    }
    public function getConnection()
    {
        return $this->connection;
    }

    public function checkUserRegister($params)
    {
        
        $userName = $this->cleanInput($params['userName']);
        $userEmail = $this->cleanInput($params['userEmail']);
        $userPass = $this->cleanInput($params['userPass']);
        $userDob = $this->cleanInput($params['userDob']);
        $userPhone = $this->cleanInput($params['userPhone']);
        //print_r($params);

        $query = "INSERT INTO user (userName ,userEmail ,userPass  ,userDob ,userPhone) 
                    values('" . $userName . "', '" . $userEmail . "', '" . $userPass . "' , '" . $userDob . "', '" . $userPhone . "')"; 
    
        if($result = mysqli_query($this->connection, $query)){
            //print_r($result);
            return $result;
            echo "Registered Succesfully";
        }
       
    }
    
    function cleanInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
